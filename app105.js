//Pure Function
function add(a, b) {
    return a + b;
}

const result = add(1, 6)
console.log("Pure Function : ", result);