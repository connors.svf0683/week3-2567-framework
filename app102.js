function a() {
    console.log("callbackFunction")
}

function sayHi(callback, fname, lname) {
    callback()
    function getFullName() {
        return fname + lname
    }
    return getFullName()
}

const message = sayHi(a, "Jimmy", "Seesang")
console.log(message)